//Task 1
//Create an array of N elements,
//populate it with arbitrary integer values.
//Output the largest value of the array, the
//smallest value of the array, the total sum of the elements,
//the arithmetic mean of all elements, output all odd values.
function createArray() {
  let n = document.getElementById('number').value;
  let textarea1 = document.getElementById('textArea1');
  let array = [];

  for (let i = 0; i < n; i++) {
    array.push(parseInt(Math.random() * 100));
  }

  textarea1.value = `Elements: \n` + array.join('\t');

  let sum = 0;
  let min = array[0];
  let max = array[0];

  for (let i = 0; i < n; i++) {
    sum += array[i];
    if (array[i] < min) {
      min = array[i];
    }
    if (array[i] > max) {
      max = array[i];
    }
  }

  let average = sum / n;

  textarea1.value += `\nMax : ${max} \nMin : ${min} \nSum : ${sum} \nAverage : ${average}`;
  textarea1.value += `\nOdd values :\n`;

  for (let i = 0; i < n; i++) {
    if (array[i] % 2 == 1) {
      textarea1.value += `${array[i]}\t`;
    }
  }
}

// Tasks 2
// Create a two-dimensional array of 5x5 elements and
// populate it with arbitrary integer values. On the main diagonal,
// replace all numbers with the sign (-) by 10, and the numbers with
// the sign (+) by the number 20.
function createDimensionalArray() {
  let textarea2 = document.getElementById('textArea2');
  let textarea3 = document.getElementById('textArea3');
  let array = [];

  textarea2.value = '';
  textarea3.value = '';

  for (let i = 0; i < 5; i++) {
    array[i] = [];
    for (let j = 0; j < 5; j++) {
      array[i][j] = parseInt(Math.random() * 100 - 50);
    }
  }

  for (let i = 0; i < 5; i++) {
    for (let j = 0; j < 5; j++) {
      textarea2.value += array[i][j] + '\t';
    }
    textarea2.value += '\n\n';
  }

  for (let i = 0; i < 5; i++) {
    if (array[i][i] < 0) {
      array[i][i] = 10;
    } else if (array[i][i] > 0) {
      array[i][i] = 20;
    }
  }

  for (let i = 0; i < 5; i++) {
    for (let j = 0; j < 5; j++) {
      textarea3.value += array[i][j] + '\t';
    }
    textarea3.value += '\n\n';
  }
}
