/* Task 1 
Create 3 variables x = 6, y = 15, and z = 4:
Perform and display the results of the following operations for these variables:
· x += y - x++ * z ; 
· z = -- x - y * 5 ; 
· y /= x + 5 % z ;  
· z = x++ + y * 5 ; 
· x = y - x++ * z ; 
Note: for each operation please use default values: x = 6, y = 15, and z = 4 */
let text = document.getElementById('textArea');
text.value = `x = 6, y = 15, z = 4
Operations:
1) x += y - x++ * z 
2) z = -- x - y * 5
3) y /= x + 5 % z 
4) z = x++ + y * 5
5) x = y - x++ * z
Results:`;

let x = 6;
let y = 15;
let z = 4;

x += y - x++ * z;
text.value += `\r\n1) ${x}`;
x = 6;

z = --x - y * 5;
text.value += `\r\n2) ${z}`;
z = 4;
x = 6;

y /= x + (5 % z);
text.value += `\r\n3) ${y.toFixed(2)}`;
y = 15;

z = x++ + y * 5;
text.value += `\r\n4) ${z}`;
z = 4;
x = 6;

x = y - x++ * z;
text.value += `\r\n5) ${x}`;

/*Task 2 
Calculate the arithmetic average value of the three integer values and display it on the screen.*/

function findAverage() {
  let number1 = +document.getElementById('number1').value;
  let number2 = +document.getElementById('number2').value;
  let number3 = +document.getElementById('number3').value;

  document.getElementById('result').value = (
    (number1 + number2 + number3) /
    3
  ).toFixed(2);
}

/*Task 3 
Write a program for calculating the volume - V and the surface area - S of the cylinder.
The volume V of the cylinder of radius-r and height-h, is calculated by the formula: V = πr 2 h.
The area S of the surface of the cylinder is calculated by the formula: S = 2π rh + 2π r 2 = 2π r (r + h). 
The results of calculations should be displayed on the screen. */

function findAreaVolume() {
  let radius = +document.getElementById('radius').value;
  let height = +document.getElementById('height').value;

  document.getElementById('volume').value = (
    Math.PI *
    radius ** 2 *
    height
  ).toFixed(2);
  document.getElementById('surfaceArea').value = (
    2 *
    Math.PI *
    radius *
    (radius + height)
  ).toFixed(2);
}
