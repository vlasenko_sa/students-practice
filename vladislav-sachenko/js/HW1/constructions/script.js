//Task 1
// Given two numbers A and B, where (A < B).
let textArea = document.getElementById('textArea');
// Display all integer numbers in interval from A to B (including borders).
function allFromAtoB() {
  let a = +document.getElementById('numberA').value;
  let b = +document.getElementById('numberB').value;

  textArea.value =
    'All integer numbers in interval from A to B (including borders).' + '\r\n';
  for (let i = a; i <= b; i++) {
    textArea.value += i + '\t';
  }
  textArea.value += '\r\n';
}
// Display all odd integer values located in the numerical interval from A to B (including borders).
function oddNumbersFromAtoB() {
  let a = +document.getElementById('numberA').value;
  let b = +document.getElementById('numberB').value;

  textArea.value +=
    'All odd integer values located in the numerical interval from A to B (including borders).' +
    '\r\n';
  for (let i = a; i <= b; i++) {
    if (i % 2 == 1) {
      textArea.value += i + '\t';
    }
  }
}
//onclick Show
function showNumbers() {
  textArea.value = '';
  allFromAtoB();
  oddNumbersFromAtoB();
}

//Task 2
//Write a program that will calculate the factorial of N, using the do-while loop.
function findFactorialN() {
  let n = Math.abs(+document.getElementById('numberN').value);
  document.getElementById('numberN').value = n;

  let i = 1;
  let factorial = 1;

  do {
    factorial *= i;
    i++;
  } while (i <= n);

  document.getElementById('result').value = factorial;
}

//Task 3
//Using the loops, draw in the browser with spaces (& nbsp) and asterisks (*):
// · Rectangle
// · Right triangle
// · Equilateral triangle
// · Rhombus.
let textAreaDraw = document.getElementById('draw');

function drawFigure() {
  //Rectangle
  for (let l = 0; l < 4; l++) {
    for (let i = 0; i < 10; i++) {
      textAreaDraw.value += '*';
    }
    textAreaDraw.value += '\r\n';
  }

  //Right triangle
  textAreaDraw.value += '\r\n';
  let b = 5;
  for (let l = 0; l < 5; l++) {
    for (let i = 0; i < b; i++) {
      textAreaDraw.value += '*';
    }
    b--;
    textAreaDraw.value += '\r\n';
  }

  //Equilateral triangle
  textAreaDraw.value += '\r\n';
  let c = 1;

  for (let l = 0; l < 2; l++) {
    for (let i = 0; i < c; i++) {
      textAreaDraw.value += '*';
    }
    c++;
    textAreaDraw.value += '\r\n';
  }
  for (let l = 0; l <= 2; l++) {
    for (let i = 0; i < c; i++) {
      textAreaDraw.value += '*';
    }
    c--;
    textAreaDraw.value += '\r\n';
  }

  //Rhombus
  let i,
    j,
    N = 7;
  let center = N / 2;
  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      if (i <= center) {
        if (j >= center - i && j <= center + i) {
          textAreaDraw.value += '*';
        } else textAreaDraw.value += ' ';
      } else {
        if (j >= center + i - N + 1 && j <= center - i + N - 1)
          textAreaDraw.value += '*';
        else textAreaDraw.value += ' ';
      }
    }
    textAreaDraw.value += '\r\n';
  }
}
